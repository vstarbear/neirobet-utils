import _ from 'lodash';
import {subscriptionsTable, subsTable} from './tables';
import round from './round';
import fillObjectFromArray from './fillObjectFromArray';
import { preFilterSubs, postFilterSubs } from "./filterSubs";
import {SIGNAL, RANGE_OPERATOR, AND_AVERAGE, REACHED_OPERATOR} from "./constants";

const isSkip = item => [AND_AVERAGE, REACHED_OPERATOR].includes(item) || (_.isString(item) && item.startsWith(RANGE_OPERATOR));

const makeConditionString = (condition = {}, defaultValue = 'true') => condition.value && condition.value.length
    ? condition.value.reduce((acc, item, index) => {
      let part;
      const nextValue = condition.value[index + 1];
      if (isSkip(item) || isSkip(nextValue)) {
        part = '';
      } else if (_.isPlainObject(item)) {
          if (item.type === 'average') {
              if (condition.value[index - 1] === REACHED_OPERATOR) {
                  const { period: movingAveragePeriod } = condition.value[index - 2];

                  part = `(previousValues[${movingAveragePeriod}] < averages[${item.period}] && currentValues[${movingAveragePeriod}] >= averages[${item.period}])`;
              } else {
                  const { period: movingAveragePeriod } = condition.value[index - 4];
                  const { period: rangePeriod } = condition.value[index - 2];
                  const { period: averagePeriod } = item;
                  const percent = _.get(condition.value[index - 3].match(/value="(\d+)"/), 1, 10);
                  part = `(currentValues[${movingAveragePeriod}] >= minimums[${rangePeriod}] && currentValues[${movingAveragePeriod}] <= ${(100 - percent) / 100} * minimums[${rangePeriod}] + ${percent / 100} * averages[${averagePeriod}])`;
              }
          } else if (item.type === 'range') {
              const { period: movingAveragePeriod } = condition.value[index - 2];

              part = `(currentValues[${movingAveragePeriod}] >= minimums[${item.period}] && currentValues[${movingAveragePeriod}] <= maximums[${item.period}])`;
          } else {
              part = `currentValues[${item.period}]`;
          }
      } else {
          part = item;
      }

      return `${acc}${part} `;
    }, '') : defaultValue;

const getPeriods = (condition = {}) => (condition.value || []).reduce((acc, item) => {
    if (item.period) {
      acc.push(+item.period);
    }

    return acc;
  }, []);

export default (user, providedSubs) => new Promise(async (resolve) => {

    const subs = providedSubs || await subsTable.all();
    const subscriptionsData = _.keyBy(await subscriptionsTable.all({ userId: user.id }), 'subId');

    const {
        capital = 1000,
        risk = 10,
        coefficient: coefficientMultiplier = 1,
        conditions,
        filters
    } = user;

    const {
        enterCondition = {},
        exitCondition = {},
        increasingCoefficientCondition = {},
        decreasingCoefficientCondition = {},
    } = conditions || {};

    const periods = _.uniq(getPeriods(enterCondition).concat(getPeriods(exitCondition)));

    const enterConditionString = makeConditionString(enterCondition);
    const exitConditionString = makeConditionString(exitCondition, 'false');
    const increasingCoefficientConditionString = makeConditionString(increasingCoefficientCondition, 'false');
    const decreasingCoefficientConditionString = makeConditionString(decreasingCoefficientCondition, 'false');

    const processedSubs = postFilterSubs(filters, preFilterSubs(filters, subs).map((sub) => {
        let coefficient = coefficientMultiplier * 1;

        let step = 0;

        let movingAverages = fillObjectFromArray(periods, []);
        let sums = fillObjectFromArray(periods, 0);
        const values = fillObjectFromArray(periods, []);

        sub.data.forEach((item) => {
            step += 1;
            const dayProfit = coefficient * (item[4] - item[1]); // close - open

            periods.forEach((period) => {
                sums[period] += dayProfit;
                movingAverages[period].push(dayProfit);
                if (step >= period) {
                    sums[period] -= movingAverages[period].shift();
                    values[period].push(sums[period] / period);
                }
            });
        });

        const averages = fillObjectFromArray(periods, period => values[period].length ? _.sum(values[period]) / values[period].length : 0);
        const minimums = fillObjectFromArray(periods, period => values[period].length ? Math.min(...values[period]) : 0);
        const maximums = fillObjectFromArray(periods, period => values[period].length ? Math.max(...values[period]) : 0);

        step = 0;

        let isCalculating = false;

        let countOfEvents = 0;
        let countOfCandles = 0;
        let profit = 0;

        const ranges = [];
        const profitPerRange = [];
        const countOfEventsPerRange = [];
        const countOfCandlesPerRange = [];

        let trendStart = 0;
        const trendFalls = [[]];

        movingAverages = fillObjectFromArray(periods, []);
        sums = fillObjectFromArray(periods, 0);

        const initialObject = fillObjectFromArray(periods, null);
        let currentValues;
        let previousValues;

        sub.data.forEach((item) => {
          previousValues = currentValues || {};
          currentValues = { ...initialObject };
          step += 1;
          const dayProfit = coefficient * (item[4] - item[1]); // close - open

          periods.forEach((period) => {
            sums[period] += dayProfit;
            movingAverages[period].push(dayProfit);
            if (step >= period) {
              sums[period] -= movingAverages[period].shift();
              currentValues[period] = sums[period] / period;
            }
          });

          if (eval(increasingCoefficientConditionString)) {
            coefficient += increasingCoefficientCondition.coefficient;
          }

          if (eval(decreasingCoefficientConditionString)) {
            coefficient -= decreasingCoefficientCondition.coefficient;
          }

          if (item.close > trendStart) {
            // close > trendStart
            trendStart = Math.max(item.close, item.open);
            if (trendFalls[trendFalls.length - 1].length) {
              trendFalls.push([]);
            }
          } else {
            trendFalls[trendFalls.length - 1].push(coefficient * item.close);
          }

          // проверяем условия входа в новый диапазон
          if (!isCalculating && eval(enterConditionString)) {
            ranges.push(0);
            profitPerRange.push(0);
            countOfEventsPerRange.push(0);
            countOfCandlesPerRange.push(0);
            isCalculating = true;
          }

          if (isCalculating) {
            // проверяем условия выхода из диапазона
            if (eval(exitConditionString)) {
              // если в диапазоне не оказалось значеений
              if (ranges[ranges.length - 1] === 0) {
                // удаляем диапазон
                ranges.pop();
                profitPerRange.pop();
                countOfEventsPerRange.pop();
                countOfCandlesPerRange.pop();
              }
              // останавливаем вычисления
              isCalculating = false;
            } else {
              // продолжаем вычисления
                ranges[ranges.length - 1] += 1;
              countOfCandles += 1;
              countOfCandlesPerRange[ranges.length - 1] += 1;
              countOfEvents += item.events;
              countOfEventsPerRange[ranges.length - 1] += item.events;
              profit += dayProfit;
              profitPerRange[ranges.length - 1] += dayProfit;
            }
          }
        });

        if (ranges.length > 0) {
          const closedRanges = isCalculating ? ranges.slice(0, ranges.length - 1) : ranges;
          const profitPerClosedRange = isCalculating ? profitPerRange.slice(0, profitPerRange.length - 1) : profitPerRange;
          const falls = trendFalls.map(fall => fall.length ? Math.max(...fall) - Math.min(...fall) : 0);
          const averageDayProfit = profit / countOfCandles;
          const averageDayProfitPerRange = closedRanges.map((range, index) => range ? profitPerClosedRange[index] / range : 0);
          const minAverageDayProfit = Math.min(...averageDayProfitPerRange);
          const maxAverageDayProfitDifference = ((minAverageDayProfit - averageDayProfit) * 100) / minAverageDayProfit;
          const closedRangesCount = closedRanges.length;
          const lastRangeProfit = _.last(profitPerRange);
          const lastProfitFractionInAverageProfit = (lastRangeProfit / averageDayProfit) * 100;
          const signal = isCalculating ? (lastProfitFractionInAverageProfit < 10 ? SIGNAL.green : (lastProfitFractionInAverageProfit < 62 ? SIGNAL.yellow : SIGNAL.red)) : SIGNAL.red;
          const ROI =  round(100 * (profit / (coefficient * countOfCandles)));

          return {
            id: sub.id,
            identifier: sub.identifier,
            name: sub.name,
            subscription: subscriptionsData[sub.id] ? 'active' :  'inactive',
            rangesCount: closedRangesCount,
            countOfCandles,
            countOfEvents,
            averageEventsCount: round(_.sum(isCalculating ? countOfEventsPerRange.slice(0, countOfEventsPerRange.length - 1) : countOfEventsPerRange) / closedRangesCount),
            averageDaysCount: round(_.sum(isCalculating ? countOfCandlesPerRange.slice(0, countOfCandlesPerRange.length - 1) : countOfCandlesPerRange) / closedRangesCount),
            profit: round(profit),
            averageProfit: round(_.sum(profitPerClosedRange) / closedRangesCount),
            averageTrendFall: round(falls.reduce((acc, item) => acc + item, 0) / falls.length),
            maxTrendFall: round(Math.max(...falls)),
            ROI,
            averageROI: round(
              closedRanges.reduce((carry, range, index) => range
                  ? carry + 100 * (profitPerClosedRange[index] / (range * coefficient))
                  : carry,
                0,
              ) / closedRangesCount,
            ),
            optimalRate: maxAverageDayProfitDifference
              ? round(capital / risk / (maxAverageDayProfitDifference / sub.modifier))
              : 0,
            lastRange: _.last(ranges),
            filters: sub.filters,
            signal
          };
        }

        return null;
      }));

    resolve(processedSubs);
  });
