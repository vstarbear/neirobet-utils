export default (subs, { averageROI, averageTrendFall }) => {
    subs.forEach((sub) => {
        sub.stars = (sub.rangesCount > 4 ? 1 : 0) + (sub.averageROI > averageROI ? 1 : 0) + (sub.maxTrendFall > averageTrendFall ? 1 : 0);
        sub.isCloseToOpeningRange = sub.lastRange > 0 && (sub.averageROI / averageROI) > 1;
    });
    return subs.sort(
        (first, second) => (first.isCloseToOpeningRange === second.isCloseToOpeningRange
            ? 5
            : 15 * (first.isCloseToOpeningRange ? -1 : 1))
            + 5 * (first.averageROI > second.averageROI ? -1 : 1)
            + 2 * (first.stars > second.stars ? -1 : 1),
    )
}