import { Table } from 'mysql-table';

export const subsTable = new Table('subs', {
    jsonFields: ['data', 'filters'],
    defaultValues: {
        filters: {
            bk: {},
            sport: {}
        },
        data: []
    }
});

export const usersTable = new Table('users', {
    jsonFields: ['conditions', 'indicators', 'filters'],
    defaultValues: {
        conditions: {},
        indicators: [],
        filters: {
            bk: {
                percent: 0,
                items: []
            },
            sport: {
                percent: 0,
                items: []
            }
        }
    }
});
export const subscriptionsTable = new Table('subscriptions');
export const coefficientsTable = new Table('coefficients');
